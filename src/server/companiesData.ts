export const LIST_OF_COMPANIES = [
  { id: "1", name: "Company_1", totalEmployees: 12, address: "ул. Сизам, д. 23" },
  { id: "2", name: "Company_2", totalEmployees: 8, address: "ул. Сизам, д. 24" },
  { id: "3", name: "Company_3", totalEmployees: 8, address: "ул. Сизам, д. 25" },
  { id: "4", name: "Company_4", totalEmployees: 4, address: "ул. Сизам, д. 26" }
];
