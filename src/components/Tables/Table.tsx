import { ActionCreatorWithPayload } from "@reduxjs/toolkit";
import React, { useEffect, useState } from "react";
import { Company } from "../../features/companies/companiesMockAPI";
import { Employee } from "../../features/employees/employeesMockAPI";
import { tableRowHeight, visibleRows } from "../../utils/constants";
import TableRow from "../TableRow/TableRow";
import { NewCompanyData } from "./CompanyNewRow";
import { NewEmployeeData } from "./EmployeeNewRow";
import styles from "./Table.module.css";

export type NewRowProps = {
  addNewRow: () => void;
  addRowWarningShow: boolean;
  styles: { [key: string]: string };
  newRow: NewEmployeeData | NewCompanyData;
  setNewRow: React.Dispatch<React.SetStateAction<NewEmployeeData | NewCompanyData>>;
};

type TableProps = {
  loading: string;
  list: Employee[] | Company[];
  checkedList: string[];
  dispatchCheckedItems: (items: string[]) => void;
  createNewItem: (value: any) => void;
  removeItems: (values: string[]) => void;
  editItem: (propId: string, value: string, itemId: string) => void;
  setCheckedItems: ActionCreatorWithPayload<string[], string>;
  AddingRow: (value: NewRowProps) => JSX.Element;
  initialNewRowState: NewEmployeeData | NewCompanyData;
  title: string;
  HeaderList: JSX.Element;
};

const Table = ({
  loading,
  list,
  checkedList,
  dispatchCheckedItems,
  createNewItem,
  removeItems,
  editItem,
  setCheckedItems,
  AddingRow,
  initialNewRowState,
  title,
  HeaderList
}: TableProps) => {
  const [allChecked, setAllChecked] = useState(false);
  const [addRowShow, setAddRowShow] = useState(false);
  const [addRowWarningShow, setAddRowWarningShow] = useState(false);
  const [startScroll, setStartScroll] = useState(0);
  const [newRow, setNewRow] = useState(initialNewRowState);

  useEffect(() => {
    if (list.length === checkedList.length && checkedList.length && !allChecked) {
      setAllChecked(true);
    } else if (list.length !== checkedList.length && allChecked) {
      setAllChecked(false);
    }
  }, [checkedList]);

  useEffect(() => {
    if (allChecked) {
      dispatchCheckedItems(list.map(({ id }) => id));
    } else if (!allChecked && checkedList.length === 0) {
      dispatchCheckedItems([]);
    } else if (!allChecked && checkedList.length !== list.length) {
      setAllChecked(false);
    } else if (!allChecked && list.length === checkedList.length && checkedList.length) {
      setAllChecked(false);
      dispatchCheckedItems([]);
    }
  }, [allChecked, list]);

  const onAllCheckChange = () => {
    setAllChecked((prev) => !prev);
  };

  const addNewRow = () => {
    const newRowValue = Object.values(newRow);
    if (newRowValue.filter((item) => !!item).length === newRowValue.length) {
      setAddRowWarningShow(false);
      createNewItem(newRow);
      setNewRow(initialNewRowState);
    } else {
      setAddRowWarningShow(true);
    }
  };

  const deleteRow = () => {
    removeItems(checkedList);
  };

  const handleEditItem = (propId: string, value: string, itemId: string) => {
    editItem(propId, value, itemId);
  };

  const onScroll = (e: React.UIEvent<HTMLDivElement, UIEvent>) => {
    if (list.length > visibleRows) {
      const elem = e.target as HTMLDivElement;
      setStartScroll(
        Math.min(list.length - visibleRows, Math.floor((elem.scrollTop < 0 ? 0 : elem.scrollTop) / tableRowHeight))
      );
    }
  };

  useEffect(() => {
    setStartScroll(0);
  }, [list]);

  const getTopHeight = () => {
    return tableRowHeight * startScroll;
  };

  const getBottomHeight = () => {
    if (list.length > visibleRows) {
      return tableRowHeight * (list.length - (startScroll + visibleRows));
    }
    return 0;
  };

  return (
    <section className={styles.wrap}>
      <h2>{title}</h2>

      <>
        <div className={styles.actionsRow}>
          <button onClick={() => setAddRowShow((prev) => !prev)}>
            {addRowShow ? "Отменить добавление" : "Добавить запись"}
          </button>
          <button onClick={deleteRow}>Удалить выделенные</button>
        </div>

        {addRowShow && (
          <AddingRow
            addNewRow={addNewRow}
            addRowWarningShow={addRowWarningShow}
            styles={{ newRowAppend: styles.newRowAppend, warnText: styles.warnText }}
            newRow={newRow}
            setNewRow={setNewRow}
          />
        )}

        <div onScroll={(e) => onScroll(e)} className={styles.container}>
          <div style={{ height: getTopHeight() }} />
          <table>
            <thead>
              <tr>
                <th scope="col">
                  <input type="checkbox" checked={allChecked} onChange={onAllCheckChange} />
                  Выделить всё
                </th>
                <>{HeaderList}</>
              </tr>
            </thead>

            <tbody>
              {loading === "success" ? (
                /* Подгрузка данных таблицы частями (эмуляция динамической подгрузки) */
                list.slice(startScroll, startScroll + visibleRows + 1).map(({ id, ...item }) => {
                  return (
                    <TableRow
                      key={id}
                      id={id}
                      item={item}
                      checkedRows={checkedList}
                      setCheckedRows={setCheckedItems}
                      onEdit={handleEditItem}
                    />
                  );
                })
              ) : (
                <tr>
                  <td colSpan={10}>Загрузка...</td>
                </tr>
              )}
            </tbody>
          </table>
          <div style={{ height: getBottomHeight() }} />
        </div>
      </>
    </section>
  );
};

export default Table;
