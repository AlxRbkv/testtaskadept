import { LIST_OF_EMPLOYEES } from "../../server/employeesData";

export type Employee = {
  id: string;
  secondName: string;
  firstName: string;
  position: string;
  companyId: string;
};

export function fetchEmployees() {
  return new Promise<{ data: Employee[] }>((resolve) => setTimeout(() => resolve({ data: LIST_OF_EMPLOYEES }), 500));
}
