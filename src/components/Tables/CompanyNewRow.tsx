import { NewRowProps } from "./Table";

export type NewCompanyData = {
  name: string;
  address: string;
};

const CompanyNewRow = ({ addNewRow, addRowWarningShow, styles, newRow, setNewRow }: NewRowProps) => {
  const newRowData = newRow as NewCompanyData;
  return (
    <table className={styles.newRowAppend}>
      <tbody>
        <tr>
          <td>
            <button onClick={addNewRow}>Доабавить</button>
            {addRowWarningShow && <p className={styles.warnText}>* Заполните все поля</p>}
          </td>
          <td>
            <input
              type="text"
              required
              placeholder="Название компании"
              value={newRowData.name}
              onChange={(e) => setNewRow((prev) => ({ ...prev, name: e.target.value }))}
            />
          </td>
          <td>
            <input
              type="text"
              required
              placeholder="Адрес"
              value={newRowData.address}
              onChange={(e) => setNewRow((prev) => ({ ...prev, address: e.target.value }))}
            />
          </td>
        </tr>
      </tbody>
    </table>
  );
};

export default CompanyNewRow;
