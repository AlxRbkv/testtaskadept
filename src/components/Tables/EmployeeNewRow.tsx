import { useEffect } from "react";
import { useAppSelector } from "../../app/hooks";
import { selectCompanies } from "../../features/companies/companiesSlice";
import { NewRowProps } from "./Table";

export type NewEmployeeData = {
  secondName: string;
  firstName: string;
  position: string;
  companyId: string;
};

const EmployeeNewRow = ({ addNewRow, addRowWarningShow, styles, newRow, setNewRow }: NewRowProps) => {
  const newRowData = newRow as NewEmployeeData;
  const companies = useAppSelector(selectCompanies);
  useEffect(() => {
    setNewRow((prev) => ({ ...prev, companyId: companies[0]?.id }));
  }, [companies]);

  return (
    <table className={styles.newRowAppend}>
      <tbody>
        <tr>
          <td>
            <button onClick={addNewRow}>Доабавить</button>
            {addRowWarningShow && <p className={styles.warnText}>* Заполните все поля</p>}
          </td>
          <td>
            <input
              type="text"
              required
              placeholder="Фамилия"
              value={newRowData.secondName}
              onChange={(e) => setNewRow((prev) => ({ ...prev, secondName: e.target.value }))}
            />
          </td>
          <td>
            <input
              type="text"
              required
              placeholder="Имя"
              value={newRowData.firstName}
              onChange={(e) => setNewRow((prev) => ({ ...prev, firstName: e.target.value }))}
            />
          </td>
          <td>
            <input
              type="text"
              required
              placeholder="Должность"
              value={newRowData.position}
              onChange={(e) => setNewRow((prev) => ({ ...prev, position: e.target.value }))}
            />
          </td>
          <td>
            <select
              placeholder="Выберите компанию"
              value={newRowData.companyId}
              onChange={(e) => setNewRow((prev) => ({ ...prev, companyId: e.target.value }))}
            >
              {companies.map((company) => (
                <option key={company.id} value={company.id}>
                  {company.name}
                </option>
              ))}
            </select>
          </td>
        </tr>
      </tbody>
    </table>
  );
};

export default EmployeeNewRow;
