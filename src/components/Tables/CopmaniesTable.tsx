import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  addNewCompany,
  deleteCompanies,
  editCompany,
  getAllCompanies,
  selectCheckedCompanies,
  selectCompanies,
  selectCompaniesLoadingStatus,
  setCheckedCompanies
} from "../../features/companies/companiesSlice";
import { newCompanyRowInitialState } from "../../utils/constants";
import CompanyNewRow, { NewCompanyData } from "./CompanyNewRow";
import Table from "./Table";

const CompaniesTable = () => {
  const dispatch = useAppDispatch();
  const companies = useAppSelector(selectCompanies);
  const checkedCompanies = useAppSelector(selectCheckedCompanies);
  const loading = useAppSelector(selectCompaniesLoadingStatus);

  useEffect(() => {
    dispatch(getAllCompanies());
  }, []);

  const dispatchCheckedCompanies = (items: string[]) => {
    dispatch(setCheckedCompanies(items));
  };

  const createCompany = (value: NewCompanyData) => {
    dispatch(addNewCompany(value));
  };

  const removeCompanies = (values: string[]) => {
    dispatch(deleteCompanies(values));
  };

  const handleEditCompany = (propId: string, value: string, companyId: string) => {
    dispatch(editCompany({ propId, value, companyId }));
  };

  const HeaderList = () => {
    return (
      <>
        <th scope="col">Название компании</th>
        <th scope="col">Кол. сотрудников</th>
        <th scope="col">Адрес</th>
      </>
    );
  };

  return (
    <Table
      title="Компании"
      loading={loading}
      list={companies}
      checkedList={checkedCompanies}
      dispatchCheckedItems={dispatchCheckedCompanies}
      createNewItem={createCompany}
      removeItems={removeCompanies}
      editItem={handleEditCompany}
      setCheckedItems={setCheckedCompanies}
      AddingRow={CompanyNewRow}
      initialNewRowState={newCompanyRowInitialState}
      HeaderList={<HeaderList />}
    />
  );
};

export default CompaniesTable;
