import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import { Employee, fetchEmployees } from "./employeesMockAPI";

export interface EmployeesState {
  employees: Employee[];
  checkedEmployees: string[];
  filteredEmployees: Employee[];
  status: "success" | "loading" | "failed";
}

const initialState: EmployeesState = {
  employees: [],
  checkedEmployees: [],
  filteredEmployees: [],
  status: "loading"
};

export const getAllEmployees = createAsyncThunk("employees/fetchEmployees", async () => {
  const response = await fetchEmployees();
  return response.data;
});

export const employeesSlice = createSlice({
  name: "employees",
  initialState,
  reducers: {
    setCheckedEmployees(state, action: PayloadAction<string[]>) {
      state.checkedEmployees = action.payload;
    },
    addNewEmployee(state, action: PayloadAction<Omit<Employee, "id">>) {
      const { secondName, firstName, position, companyId } = action.payload;
      const newEmployee = { id: (+new Date()).toString(16), secondName, firstName, position, companyId };
      state.employees = [...state.employees, newEmployee];
    },
    deleteEmployees(state, action: PayloadAction<string[]>) {
      state.employees = state.employees.filter((item) => !action.payload.includes(item.id));
    },
    filterEmployeesByCompany(state, action: PayloadAction<string[]>) {
      state.filteredEmployees = state.employees.filter((item) => action.payload.includes(item.companyId));
    },
    editEmployee(state, action: PayloadAction<{ propId: string; value: string; employeeId: string }>) {
      const { employeeId, propId, value } = action.payload;
      state.employees = state.employees.map((employee) =>
        employee.id === employeeId ? { ...employee, [propId]: value } : employee
      );
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getAllEmployees.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getAllEmployees.fulfilled, (state, action) => {
        state.status = "success";
        state.employees = action.payload;
      })
      .addCase(getAllEmployees.rejected, (state) => {
        state.status = "failed";
      });
  }
});

export const { setCheckedEmployees, addNewEmployee, deleteEmployees, filterEmployeesByCompany, editEmployee } =
  employeesSlice.actions;

export const selectEmployees = (state: RootState) => state.employees.employees;
export const selectCheckedEmployees = (state: RootState) => state.employees.checkedEmployees;
export const selectFilteredEmployees = (state: RootState) => state.employees.filteredEmployees;
export const selectEmployeesLoadingStatus = (state: RootState) => state.employees.status;

export default employeesSlice.reducer;
