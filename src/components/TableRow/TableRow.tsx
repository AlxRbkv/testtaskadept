import { ActionCreatorWithPayload } from "@reduxjs/toolkit/dist/createAction";
import { useAppDispatch } from "../../app/hooks";
import { Company } from "../../features/companies/companiesMockAPI";
import { Employee } from "../../features/employees/employeesMockAPI";
import styles from "./TableRow.module.css";

type TableRowItem = Omit<Company, "id"> | Omit<Employee, "id" | "companyId">;

type TableRowProps = {
  id: string;
  item: TableRowItem;
  checkedRows: string[];
  setCheckedRows: ActionCreatorWithPayload<string[]>;
  onEdit: (propId: string, value: string, itemId: string) => void;
};

const TableRow = ({ id, item, checkedRows, setCheckedRows, onEdit }: TableRowProps) => {
  const dispatch = useAppDispatch();
  const checked = checkedRows.includes(id);

  const onRowCheck = () => {
    const result = checkedRows.includes(id) ? checkedRows.filter((item) => item !== id) : [...checkedRows, id];
    dispatch(setCheckedRows(result));
  };

  return (
    <tr key={id} className={checked ? styles.selected : ""}>
      <td>
        <input type="checkbox" checked={checked} onChange={onRowCheck} />
      </td>
      {Object.keys(item).map((key) => {
        if (key !== "companyId") {
          return (
            <td key={key}>
              {key !== "totalEmployees" ? (
                <input type="text" value={item[key as keyof typeof item]} onChange={(e) => onEdit(key, e.target.value, id)} />
              ) : (
                <>{item[key as keyof typeof item]}</>
              )}
            </td>
          );
        } else return null;
      })}
    </tr>
  );
};

export default TableRow;
