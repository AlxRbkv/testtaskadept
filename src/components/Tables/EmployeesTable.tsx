import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  addEmployeeToCompany,
  deleteEmployeesFromCompany,
  selectCheckedCompanies,
  selectCompanies
} from "../../features/companies/companiesSlice";
import { Employee } from "../../features/employees/employeesMockAPI";
import {
  addNewEmployee,
  deleteEmployees,
  editEmployee,
  filterEmployeesByCompany,
  getAllEmployees,
  selectCheckedEmployees,
  selectEmployees,
  selectEmployeesLoadingStatus,
  selectFilteredEmployees,
  setCheckedEmployees
} from "../../features/employees/employeesSlice";
import { newEmployeeRowInitialState } from "../../utils/constants";
import EmployeeNewRow from "./EmployeeNewRow";
import Table from "./Table";

const EmployeesTable = () => {
  const dispatch = useAppDispatch();
  const employees = useAppSelector(selectEmployees);
  const filteredEmployees = useAppSelector(selectFilteredEmployees);
  const companies = useAppSelector(selectCompanies);
  const checkedEmployees = useAppSelector(selectCheckedEmployees);
  const checkedCompanies = useAppSelector(selectCheckedCompanies);
  const loading = useAppSelector(selectEmployeesLoadingStatus);

  useEffect(() => {
    dispatch(getAllEmployees());
  }, []);

  useEffect(() => {
    dispatch(filterEmployeesByCompany(checkedCompanies));
  }, [checkedCompanies, employees, companies]);

  const createEmployee = (value: Omit<Employee, "id">) => {
    dispatch(addNewEmployee(value));
    dispatch(addEmployeeToCompany(value.companyId));
  };

  const getDeletedEmployeesData = () => {
    const deletedEmployees = filteredEmployees.filter((employee) => checkedEmployees.includes(employee.id));
    const deletedCompanyId = deletedEmployees.map((employee) => String(employee.companyId));
    const result = deletedCompanyId.reduce((acc: { [key: string]: number }, el) => {
      acc[el] = (acc[el] || 0) + 1;
      return acc;
    }, {});
    return result;
  };

  const removeEmployees = (values: string[]) => {
    dispatch(deleteEmployees(values));
    dispatch(deleteEmployeesFromCompany(getDeletedEmployeesData()));
  };

  const dispatchCheckedEmployees = (items: string[]) => {
    dispatch(setCheckedEmployees(items));
  };

  const handleEditEmployee = (propId: string, value: string, employeeId: string) => {
    dispatch(editEmployee({ propId, value, employeeId }));
  };

  const HeaderList = () => {
    return (
      <>
        <th scope="col">Фамилия</th>
        <th scope="col">Имя</th>
        <th scope="col">Должность</th>
      </>
    );
  };

  return (
    <>
      {checkedCompanies.length !== 0 && (
        <Table
          title="Сотрудники"
          loading={loading}
          list={filteredEmployees}
          checkedList={checkedEmployees}
          dispatchCheckedItems={dispatchCheckedEmployees}
          createNewItem={createEmployee}
          removeItems={removeEmployees}
          editItem={handleEditEmployee}
          setCheckedItems={setCheckedEmployees}
          AddingRow={EmployeeNewRow}
          initialNewRowState={newEmployeeRowInitialState}
          HeaderList={<HeaderList />}
        />
      )}
    </>
  );
};

export default EmployeesTable;
