import "./App.css";
import CopmaniesTable from "./components/Tables/CopmaniesTable";
import EmployeesTable from "./components/Tables/EmployeesTable";

function App() {
  return (
    <div className="App">
      <section className="tables__wrap">
        <CopmaniesTable />
        <EmployeesTable />
      </section>
    </div>
  );
}

export default App;
