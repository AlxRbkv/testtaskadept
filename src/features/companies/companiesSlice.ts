import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import { NewCompanyData } from "../../components/Tables/CompanyNewRow";
import { Company, fetchCompanies } from "./companiesMockAPI";

export interface CompaniesState {
  companies: Company[];
  checkedCompanies: string[];
  status: "success" | "loading" | "failed";
}

const initialState: CompaniesState = {
  companies: [],
  checkedCompanies: [],
  status: "loading"
};

export const getAllCompanies = createAsyncThunk("companies/getAllCompanies", async () => {
  const response = await fetchCompanies();
  return response.data;
});

export const companiesSlice = createSlice({
  name: "companies",
  initialState,
  reducers: {
    setCheckedCompanies(state, action: PayloadAction<string[]>) {
      state.checkedCompanies = action.payload;
    },
    addNewCompany(state, action: PayloadAction<NewCompanyData>) {
      const { name, address } = action.payload;
      state.companies = [...state.companies, { id: (+new Date()).toString(16), name, totalEmployees: 0, address }];
    },
    deleteCompanies(state, action: PayloadAction<string[]>) {
      state.companies = state.companies.filter((item) => !action.payload.includes(item.id));
      state.checkedCompanies = state.companies.filter((item) => !action.payload.includes(item.id)).map((item) => item.id);
    },
    addEmployeeToCompany(state, action: PayloadAction<string>) {
      state.companies = state.companies.map((company) =>
        company.id === action.payload ? { ...company, totalEmployees: ++company.totalEmployees } : company
      );
    },
    editCompany(state, action: PayloadAction<{ propId: string; value: string; companyId: string }>) {
      const { companyId, propId, value } = action.payload;
      state.companies = state.companies.map((company) =>
        company.id === companyId ? { ...company, [propId]: value } : company
      );
    },
    deleteEmployeesFromCompany(state, action: PayloadAction<{ [key: string]: number }>) {
      for (let key in action.payload) {
        const companyId = state.companies.find((company) => company.id === key)?.id;
        if (companyId) {
          const index = state.companies.indexOf(
            state.companies.find((company) => company.id === companyId) || state.companies[0]
          );
          state.companies[index].totalEmployees = state.companies[index].totalEmployees - action.payload[key];
        }
      }
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getAllCompanies.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getAllCompanies.fulfilled, (state, action) => {
        state.status = "success";
        state.companies = action.payload;
      })
      .addCase(getAllCompanies.rejected, (state) => {
        state.status = "failed";
      });
  }
});

export const {
  setCheckedCompanies,
  addNewCompany,
  deleteCompanies,
  addEmployeeToCompany,
  deleteEmployeesFromCompany,
  editCompany
} = companiesSlice.actions;

export const selectCompanies = (state: RootState) => state.companies.companies;
export const selectCheckedCompanies = (state: RootState) => state.companies.checkedCompanies;
export const selectCompaniesLoadingStatus = (state: RootState) => state.companies.status;

export default companiesSlice.reducer;
