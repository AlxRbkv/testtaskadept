export const tableRowHeight = 56;
export const visibleRows = 10;
export const newEmployeeRowInitialState = { firstName: "", secondName: "", position: "", companyId: "1" };
export const newCompanyRowInitialState = { name: "", address: "" };
