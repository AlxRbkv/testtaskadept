import { LIST_OF_COMPANIES } from "../../server/companiesData";

export type Company = {
  id: string;
  name: string;
  totalEmployees: number;
  address: string;
};

export function fetchCompanies() {
  return new Promise<{ data: Company[] }>((resolve) => setTimeout(() => resolve({ data: LIST_OF_COMPANIES }), 100));
}
